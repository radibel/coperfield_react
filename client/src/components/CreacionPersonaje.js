import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, ListGroup, Button } from "react-bootstrap";

import { Stage, Layer, Image, Group, Text, Rect, Transformer } from 'react-konva';
import './CreacionPersonaje.css';

import URLImage from './URLImage'
import Categorias from './CreacionPersonaje/Categorias'

//Actions
import {    
    getCategoryCuerpos,
    getCategoryBrazos,
    getCategoryExpresiones,
    getCategorypeinados,

    setSelectCuerpo,
    setSelectBrazo,
    setSelectExpresion,
    setSelectpeinado,
    guardarPersonaje,
    
  } from '../actions/PersonajeActions';
  //Selectors
  import {
    PersonajeCuerposSelector,
    PersonajeBrazosSelector,
    PersonajeExpresionesSelector,
    PersonajepeinadosSelector,
    PersonajeCompletoSelector, 
  } from '../selectors/PersonajeSelectors';

  const allItems_url = "http://192.168.1.103:8080/item/all"
//   cuerpos
  const byCatItems_url1 = "http://localhost:8080/item/getAllByCategoryId?categoryId=1"
//   brazos
  const byCatItems_url2 = "http://localhost:8080/item/getAllByCategoryId?categoryId=2"
// expresion
  const byCatItems_url3 = "http://localhost:8080/item/getAllByCategoryId?categoryId=3"
//   peinados
  const byCatItems_url4 = "http://localhost:8080/item/getAllByCategoryId?categoryId=4"


// Stage Component
// @author      Radi
// @since       1.0
// @created     4.07.2019
class CreacionPersonaje extends Component {
    constructor() {
        super()

        this.state = {
          nombrePersonaje: "",
          personaje: {},
          
        }
    }

    

    // --- START HERE ---
    componentDidMount = () =>{
      
    }

    // GURADR PERSONAJE
    guardarPersonaje = () => {      
      let _body = {
        element:{}, 
        elementDesign:[]
       }          
      let _element = { 
        name: "",
        user: {
          userId: 0
        }
      }     

      _element.name = this.state.nombrePersonaje //this.props.nombrePersonaje
      _element.user.userId = 1 //this.props.userId
      _body.element = _element

      this.props.personajeCompleto.personajeCompleto.forEach((item) => {
        let _elementDesign = {
          item:{
            "id": 0
          },          
          properties:{}
        }

        _elementDesign.item.id = item.id
        _elementDesign.properties.posX = 55
        _elementDesign.properties.posY = 54
        _elementDesign.properties = JSON.stringify(_elementDesign.properties)
        _body.elementDesign.push(_elementDesign)
      })

      this.props.personajeCompleto.personajeBrazos.forEach((item) => {
        let _elementDesign = {
          item:{
            "id": 0
          },          
          properties:{}
        }

        _elementDesign.item.id = item.id
        _elementDesign.properties.posX = 55
        _elementDesign.properties.posY = 54       
        _elementDesign.properties = JSON.stringify(_elementDesign.properties)
        _body.elementDesign.push(_elementDesign)
      })
      
      this.props.guardarPersonaje(_body)

    }

    upperCaseF = (e) => {      
      this.setState({nombrePersonaje: e.target.value.toUpperCase()})
    }

    render() {
        return (
            <div>
            <Row>
                <Categorias></Categorias>
                <Col lg={3} md={3} className="columna">

                  <Button onClick={()=> this.guardarPersonaje()}>Guardar</Button>
                  <p></p>
                  <input type="text" value={this.state.nombrePersonaje} className="nombrePersonajeTexto" 
                    onChange={(value) => this.upperCaseF(value)} id="nombrePersonaje" placeholder="Dale un nombre!"></input>

                </Col>
            </Row>            
            <Stage id="stage1" className="canvas" width={window.innerWidth} height={window.innerHeight}>      
                <Layer name="canvas1">
                    <URLImage src="https://www.psdgraphics.com/file/open-book.jpg" posX={-500} posY={-100} scaleX={.4} scaleY={.3} />        
                    <Text name="temp" text="CREA TU PERSONAJE" x={800} y={100}></Text>
                                       
                        <Group className="canvas" draggable={true} x={600} y={200}>
                        {/* Aqui se dibuja el monito */}
                        { this.props.personajeCompleto.personajeCompleto.map(item =>
                            <URLImage key={item.id} src={item.properties} scaleX={.5} scaleY={.5} />)}

                          {/* BRAZOS */}
                        { this.props.personajeCompleto.personajeBrazos.map(item =>
                        <URLImage key={item.id} src={item.properties} scaleX={.5} scaleY={.5} />)}
                    </Group>
                    {/* <ColoredRect />
                    <Handler /> */}
                </Layer>
            </Stage>
            </div>
        );
    }
}

class ColoredRect extends React.Component {
    constructor() {
        super()

        this.state = {
            test: 0,
        }
    }
    handleTransform = () => {
      console.log("transformed");
      // we can read attrs here and send them to store
    };
    handleClick = () => {
      this.setState({test: this.state.test + 40})
      // we can read attrs here and send them to store
    };
    render() {
      return (
        <Rect
          name="rectange-name"
          x={100}
          y={100}
          width={200}
          height={10}
          fill="red"
          onClick={this.handleClick}
        //   onTransform={this.handleTransform} 
          rotation={this.state.test} 
          offsetX={5}
          offsetY={5}
        />        
      );
    }
  }



class Handler extends React.Component {
    componentDidMount() {      
      const stage = this.transformer.getStage();
      const rectangle = stage.findOne(".rectange-name");   
      // const rectangle = stage.findOne(".canvas1");   
      this.transformer.attachTo(rectangle);
      this.transformer.getLayer().batchDraw();
         
    }

    render() {
      return (
        <Transformer
        // rotationSnaps={[0, 90, 180, 270]}
        anchorCornerRadius={45}
        anchorSize={10}
        resizeEnabled={false}
          ref={node => {
            this.transformer = node;
          }}
        />
      );
    }
  }

  


//Redux stuff
function mapStateToProps(state) {
    return {
        cuerpos: PersonajeCuerposSelector(state),
        brazos: PersonajeBrazosSelector(state),
        expresiones: PersonajeExpresionesSelector(state),
        peinados: PersonajepeinadosSelector(state),
        personajeCompleto: PersonajeCompletoSelector(state),
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCategoryCuerpos,
        getCategoryBrazos,
        getCategoryExpresiones,
        getCategorypeinados,

        setSelectCuerpo,
        setSelectBrazo,
        setSelectExpresion,
        setSelectpeinado,
        guardarPersonaje,
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(CreacionPersonaje)



// // the first very simple and recommended way:
// const LionImage = () => {
//   const [image] = useImage('https://konvajs.org/assets/lion.png');
//   return <Image image={image} />;
// };