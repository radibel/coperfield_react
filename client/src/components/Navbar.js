import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Navbar, Nav, NavDropdown, FormControl, Button, Form  } from "react-bootstrap";

import { Stage, Layer, Image, Group, Text, Rect } from 'react-konva';

import URLImage from './URLImage'

//Actions
import {    

    
  } from '../actions/PersonajeActions';
  //Selectors
  import {

    
  } from '../selectors/PersonajeSelectors';




// Stage Component
// @author      Radi
// @since       1.0
// @created     4.07.2019
class NavbarComponent extends Component {
    constructor() {
        super()

        this.state = {
        }
    }

    // --- START HERE ---
    componentDidMount = () =>{       

        
    }
    render() {        

        return (
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#link">Link</Nav.Link>
                    <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                    </NavDropdown>
                    </Nav>
                    <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}


//Redux stuff
function mapStateToProps(state) {
    return {
        
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(NavbarComponent)



// // the first very simple and recommended way:
// const LionImage = () => {
//   const [image] = useImage('https://konvajs.org/assets/lion.png');
//   return <Image image={image} />;
// };



