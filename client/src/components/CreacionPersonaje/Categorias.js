import React, { Component } from 'react';
import { connect, Provider } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Row, Col, ListGroup, Button } from "react-bootstrap";

import { Stage, Layer, Image, Group, Text, Rect, Transformer } from 'react-konva';

//Actions
import {    
    getCategoryCuerpos,
    getCategoryBrazos,
    getCategoryExpresiones,
    getCategorypeinados,

    setSelectCuerpo,
    setSelectBrazo,
    setSelectExpresion,
    setSelectpeinado,    
    
  } from '../../actions/PersonajeActions';
  //Selectors
  import {
    PersonajeCuerposSelector,
    PersonajeBrazosSelector,
    PersonajeExpresionesSelector,
    PersonajepeinadosSelector,    
  } from '../../selectors/PersonajeSelectors';

  const allItems_url = "http://192.168.1.103:8080/item/all"
//   cuerpos
  const byCatItems_url1 = "http://localhost:8080/item/getAllByCategoryId?categoryId=1"
//   brazos
  const byCatItems_url2 = "http://localhost:8080/item/getAllByCategoryId?categoryId=2"
// expresion
  const byCatItems_url3 = "http://localhost:8080/item/getAllByCategoryId?categoryId=3"
//   peinados
  const byCatItems_url4 = "http://localhost:8080/item/getAllByCategoryId?categoryId=4"


// Categorias Component
// @author      Radi
// @since       1.0
// @created     5.15.2019
class Categorias extends Component {
    constructor() {
        super()

        this.state = {
        }
    }

    // --- START HERE ---
    componentDidMount = () =>{ 
        let Body1 = { url: byCatItems_url1}
        let Body2 = { url: byCatItems_url2}
        let Body3 = { url: byCatItems_url3}
        let Body4 = { url: byCatItems_url4}

        this.props.getCategoryCuerpos(Body1)
        this.props.getCategoryBrazos(Body2)
        this.props.getCategoryExpresiones(Body3)
        this.props.getCategorypeinados(Body4)
    }

    upperCaseF = (e) => {       
      // console.log(e.target.value)      
    }

    render() {
        return (
            <div>
            <Col lg={3} md={3} className="columna">                
                    <ListGroup>
                        <ListGroup.Item variant='info' >CUERPOS</ListGroup.Item>
                        { this.props.cuerpos.map(item => 
                        <ListGroup.Item key={item.index} variant='light' action onClick={()=> this.props.setSelectCuerpo(item.index)}>{item.atributes}</ListGroup.Item>)}                    
                    </ListGroup>                    
                </Col>
                <Col lg={3} md={3} className="columna">
                <ListGroup>
                    <ListGroup.Item variant='info'>BRAZOS</ListGroup.Item>
                        { this.props.brazos.map(item => 
                        <ListGroup.Item key={item.index} variant='light' action onClick={()=> this.props.setSelectBrazo(item.index)}>{item.atributes}</ListGroup.Item>)}                    
                    </ListGroup>
                </Col>
                <Col lg={3} md={3} className="columna">
                <ListGroup>  
                    <ListGroup.Item variant='info'>EXPRESION</ListGroup.Item>
                        { this.props.expresiones.map(item => 
                        <ListGroup.Item key={item.index} variant='light' action onClick={()=> this.props.setSelectExpresion(item.index)}>{item.atributes}</ListGroup.Item>)}                    
                    </ListGroup>
                </Col>
                <Col lg={3} md={3} className="columna">
                <ListGroup>  
                    <ListGroup.Item variant='info'>PEINADOS</ListGroup.Item>
                        { this.props.peinados.map(item => 
                        <ListGroup.Item key={item.index} variant='light' action onClick={()=> this.props.setSelectpeinado(item.index)}>{item.atributes}</ListGroup.Item>)}                    
                    </ListGroup>
            </Col>
         </div>  
        );
    }
}  


//Redux stuff
function mapStateToProps(state) {
    return {
        cuerpos: PersonajeCuerposSelector(state),
        brazos: PersonajeBrazosSelector(state),
        expresiones: PersonajeExpresionesSelector(state),
        peinados: PersonajepeinadosSelector(state),        
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getCategoryCuerpos,
        getCategoryBrazos,
        getCategoryExpresiones,
        getCategorypeinados,

        setSelectCuerpo,
        setSelectBrazo,
        setSelectExpresion,
        setSelectpeinado,        
    }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Categorias)