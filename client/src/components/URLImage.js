import React, { Component } from 'react';
import { Stage, Layer, Image, Group, Transformer } from 'react-konva';



// custom component that will handle loading image from url
// you may add more logic here to handle "loading" state 
// or if loading is failed
// VERY IMPORTANT NOTES:
// at first we will set image state to null
// and then we will set it to native image instance when it is loaded
class URLImage extends React.Component {
    state = {
      image: null,
      scaleX: this.props.scaleX,
      scaleY: this.props.scaleY,
      posX: this.props.posX,
      posY: this.props.posY
    };
    componentDidMount() {
      this.loadImage();
    }
    componentDidUpdate(oldProps) {      
      if (oldProps.src !== this.props.src) {
        this.loadImage();
      }
    }
    componentWillUnmount() {
      this.image.removeEventListener('load', this.handleLoad);
    }
    loadImage() {
      // save to "this" to remove "load" handler on unmount
      this.image = new window.Image();      
      this.image.src = this.props.src;
      this.image.addEventListener('load', this.handleLoad);
    }
    handleLoad = () => {
      // after setState react-konva will update canvas and redraw the layer
      // because "image" property is changed
      this.setState({
        image: this.image
      });
      
      // if you keep same image object during source updates
      // you will have to update layer manually:
      // this.imageNode.getLayer().batchDraw();
    };


    render() {
      return (        
        
        <Image          
          x={this.props.posX}
          y={this.props.posY}
          image={this.state.image}
          ref={node => {
            this.imageNode = node;
          }}
          scaleX={this.state.scaleX}
          scaleY={this.state.scaleY}
        />        
      );
    }
  }

  // 'x: ' + rect.x(),
  // 'y: ' + rect.y(),
  // 'rotation: ' + rect.rotation(),
  // 'width: ' + rect.width(),
  // 'height: ' + rect.height(),
  // 'scaleX: ' + rect.scaleX(),
  // 'scaleY: ' + rect.scaleY()
  


  export default URLImage; 