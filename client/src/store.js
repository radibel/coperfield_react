import { createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
//Root reducer
import rootReducer from "./reducers";
//Initial State
import { initialState as personaje } from './reducers/PersonajeReducer';

const defaultState = {
    personaje
};

const store = createStore(
    rootReducer,
    defaultState,
    applyMiddleware(thunk)
);

export default store;


