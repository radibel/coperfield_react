import { apiThunkHelper, apiThunkHelperPost } from './ApiHelper';

// CUERPO
export const RETRIEVE_CUERPOS = {
    BEGIN: 'RETRIEVE_CUERPOS.BEGIN',
    FAILURE: 'RETRIEVE_CUERPOS.FAILURE',
    SUCCESS: 'RETRIEVE_CUERPOS.SUCCESS'
};

const RETRIEVE_CUERPOS_TYPES = [
    RETRIEVE_CUERPOS.BEGIN,
    RETRIEVE_CUERPOS.SUCCESS,
    RETRIEVE_CUERPOS.FAILURE
];

export const SET_SELECTED_CUERPO = 'SET_SELECTED_CUERPO';
export function setSelectCuerpo(index) {
    return {
        payload: index,
        type: SET_SELECTED_CUERPO
    };
}

export function getCategoryCuerpos(body) {    
    return (dispatch) => {
        const requestObject = {
            Name: 'items',
            Path: '/all',
            Init: {body}
        };        
        return apiThunkHelper(dispatch, RETRIEVE_CUERPOS_TYPES, requestObject);
    };
}

// BRAZOS
export const RETRIEVE_BRAZOS = {
    BEGIN: 'RETRIEVE_BRAZOS.BEGIN',
    FAILURE: 'RETRIEVE_BRAZOS.FAILURE',
    SUCCESS: 'RETRIEVE_BRAZOS.SUCCESS'
};

const RETRIEVE_BRAZOS_TYPES = [
    RETRIEVE_BRAZOS.BEGIN,
    RETRIEVE_BRAZOS.SUCCESS,
    RETRIEVE_BRAZOS.FAILURE
];

export const SET_SELECTED_BRAZO = 'SET_SELECTED_BRAZO';
export function setSelectBrazo(index) {
    return {
        payload: index,
        type: SET_SELECTED_BRAZO
    };
}

export function getCategoryBrazos(body) {    
    return (dispatch) => {
        const requestObject = {
            Name: 'items',
            Path: '/all',
            Init: {body}
        };        
        return apiThunkHelper(dispatch, RETRIEVE_BRAZOS_TYPES, requestObject);
    };
}

// EXPRESIONES
export const RETRIEVE_EXPRESIONES = {
    BEGIN: 'RETRIEVE_EXPRESIONES.BEGIN',
    FAILURE: 'RETRIEVE_EXPRESIONES.FAILURE',
    SUCCESS: 'RETRIEVE_EXPRESIONES.SUCCESS'
};

const RETRIEVE_EXPRESIONES_TYPES = [
    RETRIEVE_EXPRESIONES.BEGIN,
    RETRIEVE_EXPRESIONES.SUCCESS,
    RETRIEVE_EXPRESIONES.FAILURE
];

export const SET_SELECTED_EXPRESION = 'SET_SELECTED_EXPRESION';
export function setSelectExpresion(index) {
    return {
        payload: index,
        type: SET_SELECTED_EXPRESION
    };
}

export function getCategoryExpresiones(body) {    
    return (dispatch) => {
        const requestObject = {
            Name: 'items',
            Path: '/all',
            Init: {body}
        };        
        return apiThunkHelper(dispatch, RETRIEVE_EXPRESIONES_TYPES, requestObject);
    };
}

export const RETRIEVE_peinados = {
    BEGIN: 'RETRIEVE_peinados.BEGIN',
    FAILURE: 'RETRIEVE_peinados.FAILURE',
    SUCCESS: 'RETRIEVE_peinados.SUCCESS'
};

const RETRIEVE_peinados_TYPES = [
    RETRIEVE_peinados.BEGIN,
    RETRIEVE_peinados.SUCCESS,
    RETRIEVE_peinados.FAILURE
];

export const SET_SELECTED_peinado = 'SET_SELECTED_peinado';
export function setSelectpeinado(index) {
    return {
        payload: index,
        type: SET_SELECTED_peinado
    };
}

export function getCategorypeinados(body) {    
    return (dispatch) => {
        const requestObject = {
            Name: 'items',
            Path: '/all',
            Init: {body}
        };        
        return apiThunkHelper(dispatch, RETRIEVE_peinados_TYPES, requestObject);
    };
}

// TODOS
export function getItemsAll(body) {    
    return (dispatch) => {
        const requestObject = {
            Name: 'items',
            Path: '/all',
            Init: {body}
        };        
        return apiThunkHelper(dispatch, RETRIEVE_peinados_TYPES, requestObject);
    };
}


// GUARDAR PERSONAJE
export const SEND_GUARDARPERSONAJE = {
    BEGIN: 'SEND_GUARDARPERSONAJE.BEGIN',
    FAILURE: 'SEND_GUARDARPERSONAJE.FAILURE',
    SUCCESS: 'SEND_GUARDARPERSONAJE.SUCCESS'
};

const SEND_GUARDARPERSONAJE_TYPES = [
    SEND_GUARDARPERSONAJE.BEGIN,
    SEND_GUARDARPERSONAJE.SUCCESS,
    SEND_GUARDARPERSONAJE.FAILURE
];

export function guardarPersonaje(body) {    
    return (dispatch) => {
        const requestObject = {
            Name: 'element/design/add/wrapper',
            Path: '/personaje',
            Init: {body}
        };        
        return apiThunkHelperPost(dispatch, SEND_GUARDARPERSONAJE_TYPES, requestObject);
    };
}





// export const RETRIEVE_BILLS = {
//     BEGIN: 'RETRIEVE_BILLS.BEGIN',
//     FAILURE: 'RETRIEVE_BILLS.FAILURE',
//     SUCCESS: 'RETRIEVE_BILLS.SUCCESS'
// };

// const RETRIEVE_BILLS_TYPES = [
//     RETRIEVE_BILLS.BEGIN,
//     RETRIEVE_BILLS.SUCCESS,
//     RETRIEVE_BILLS.FAILURE
// ];

// export function getBills(locationId) {
//     return (dispatch) => {
//         const requestObject = {
//             Name: 'bills',
//             Path: `/bills/${locationId}`,
//             Init: {}
//         };
//         return apiThunkHelper(dispatch, RETRIEVE_BILLS_TYPES, requestObject);
//     };
// }


// export const SET_SELECTED_LOCATION = 'SET_SELECTED_LOCATION';
// export function setSelectedLocation(id) {
//     return {
//         payload: id,
//         type: SET_SELECTED_LOCATION
//     };
// }



// export const SET_SELECTED_ALL_BILLS = 'SET_SELECTED_ALL_BILLS';
// export function setSelectedAllBills(selected) {
//     return {
//         payload: selected,
//         type: SET_SELECTED_ALL_BILLS
//     };
// }