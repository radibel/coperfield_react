// import {API} from 'aws-amplify/lib/index';
import axios from "axios";

const toledosUrl = "http://192.168.1.103:8080/"
const headers = {
    'Content-Type': 'application/json',
    'Authorization': 'JWT fefege...' 
}

// http://192.168.1.103:8080/element/design/getAllByElementId?elementId=2
// http://192.168.1.103:8080/user/add?name=radi&email=a@a.com
// http://192.168.1.103:8080/element/getAllByUserId?userId=1

// const allItems_url = "http://192.168.1.103:8080/item/all"
// const byCatItems_url = "http://192.168.1.103:8080/item/all"

export const apiThunkHelper = (dispatch, types, requestObject) => {
    const [requestType, successType, errorType] = types;    
    dispatch({
        type: requestType
    });
    return axios.get(requestObject.Init.body.url).then(response => {        
        dispatch({
            type: successType,
            payload: response.data
        });
     }).catch((response) => {
        dispatch({
            type: errorType
        });
    })
}

export const apiThunkHelperPost = (dispatch, types, requestObject) => {
    const [requestType, successType, errorType] = types;
    dispatch({
        type: requestType
    });    
    return axios.post(toledosUrl + requestObject.Name + requestObject.Path, JSON.stringify(requestObject.Init.body), {headers: headers}).then(response => {        
        dispatch({
            type: successType,
            payload: response.data
        });
     }).catch((response) => {
        dispatch({
            type: errorType
        });
    })
}


    // return API.get(requestObject.Name, requestObject.Path, requestObject.Init).then((response) => {
    //     dispatch({
    //         type: successType,
    //         payload: response
    //     });
    // }).catch((response) => {
    //     dispatch({
    //         type: errorType
    //     });
    // })