import { createSelector } from 'reselect'

export const PersonajeSelector = (state) => {
    return state.personaje;
};

export const PersonajeCuerposSelector = createSelector(
    [PersonajeSelector],
    (personaje) => {
        return personaje.cuerpos;
    }
);

export const PersonajeBrazosSelector = createSelector(
    [PersonajeSelector],
    (personaje) => {
        return personaje.brazos;
    }
);

export const PersonajeExpresionesSelector = createSelector(
    [PersonajeSelector],
    (personaje) => {
        return personaje.expresiones;
    }
);

export const PersonajepeinadosSelector = createSelector(
    [PersonajeSelector],
    (personaje) => {
        return personaje.peinados;
    }
);

export const PersonajeIsFetchingSelector = createSelector(
    [PersonajeSelector],
    (personaje) => {
        return personaje.isFetching;
    }
);

export const PersonajeCompletoSelector = createSelector(
    [PersonajeCuerposSelector,
     PersonajeBrazosSelector,
     PersonajeExpresionesSelector,
     PersonajepeinadosSelector],
    (cuerpos, brazos, expresiones, peinados) => {
        let _personaje = { personajeCompleto: [], personajeBrazos: [] }; 

        cuerpos.forEach((item) => {
            if (item.selected) {
                _personaje.personajeCompleto.push(item)
            }
        })
        brazos.forEach((item) => {
            if (item.selected) {
                _personaje.personajeBrazos.push(item)
            }
        })
        expresiones.forEach((item) => {
            if (item.selected) {
                _personaje.personajeCompleto.push(item)
            }
        })
        peinados.forEach((item) => {
            if (item.selected) {
                _personaje.personajeCompleto.push(item)
            }
        })

        return _personaje;
    }
);