import {
    RETRIEVE_BRAZOS, 
    RETRIEVE_CUERPOS, 
    RETRIEVE_EXPRESIONES, 
    RETRIEVE_peinados, 
    SET_SELECTED_CUERPO,
    SET_SELECTED_BRAZO,
    SET_SELECTED_EXPRESION,
    SET_SELECTED_peinado, 
    SEND_GUARDARPERSONAJE,
} from '../actions/PersonajeActions';

export const initialState = {    
    brazos: [],
    peinados: [],
    cuerpos: [],
    expresiones: [],
    isFetching: false,
    items: [],
    personajeId: 0,
    nombrePersonaje: "",
    personajeGuardado: []
}; 
 
function personaje(state = initialState, action) {
    switch (action.type) {
        // CUERPOS
        case RETRIEVE_CUERPOS.BEGIN:
            return {
                ...state,
                isFetching: true
            };
        case RETRIEVE_CUERPOS.SUCCESS:
                let cuerpoi = 0;
            return {
                ...state,
                isFetching: false,                
                cuerpos: action.payload.map((item) => {                    
                    return {
                        ...item,
                        selected: false,
                        index: cuerpoi++
                    }
                })
            };
        case RETRIEVE_CUERPOS.ERROR:
            return {
                ...state,
                isFetching: false
            };
        case SET_SELECTED_CUERPO:
            return {
                ...state,
                cuerpos: setSelectedCuerpo(state.cuerpos, action.payload)
            };
        // BRAZOS
        case RETRIEVE_BRAZOS.BEGIN:
            return {
                ...state,
                isFetching: true
            };
        case RETRIEVE_BRAZOS.SUCCESS:
                let brazoi = 0;
            return {
                ...state,
                isFetching: false,                
                brazos: action.payload.map((item) => {                    
                    return {
                        ...item,
                        selected: false,
                        index: brazoi++
                    }
                })
            };
        case RETRIEVE_BRAZOS.ERROR:
            return {
                ...state,
                isFetching: false
            };
        case SET_SELECTED_BRAZO:
                return {
                    ...state,
                    brazos: setSelectedBrazo(state.brazos, action.payload)
                };
        // EXPRESIONES    
        case RETRIEVE_EXPRESIONES.BEGIN:
            return {
                ...state,
                isFetching: true
            };
        case RETRIEVE_EXPRESIONES.SUCCESS:
                let expresioni = 0;
            return {
                ...state,
                isFetching: false,                
                expresiones: action.payload.map((item) => {                    
                    return {
                        ...item,
                        selected: false,
                        index: expresioni++
                    }
                })
            };
        case RETRIEVE_EXPRESIONES.ERROR:
            return {
                ...state,
                isFetching: false
            };
        case SET_SELECTED_EXPRESION:
                return {
                    ...state,
                    expresiones: setSelectedExpresion(state.expresiones, action.payload)
                };    
        // peinados    
        case RETRIEVE_peinados.BEGIN:
            return {
                ...state,
                isFetching: true
            };
        case RETRIEVE_peinados.SUCCESS:
                let peinadoi = 0;                
            return {
                ...state,
                isFetching: false,                
                peinados: action.payload.map((item) => {                    
                    return {
                        ...item,
                        selected: false,
                        index: peinadoi++
                    }
                })
            };
        case RETRIEVE_peinados.ERROR:
            return {
                ...state,
                isFetching: false
            };
        case SET_SELECTED_peinado:
            return {
                ...state,
                peinados: setSelectedpeinado(state.peinados, action.payload)
            };
        // GUARDAR PERSONAJE
        case SEND_GUARDARPERSONAJE.BEGIN:
            return {
                ...state,
                isFetching: true
            };
        case SEND_GUARDARPERSONAJE.SUCCESS:
            console.log(action.payload)
            return {
                ...state,
                isFetching: false,                
                personajeId: action.payload
            };
        case SEND_GUARDARPERSONAJE.ERROR:
            return {
                ...state,
                isFetching: false
            };
        default:
            return state;
    }

}

const setSelectedCuerpo = (items, index) => {
    items.forEach(element => {
        element.selected = false
    });
    items[index].selected = !items[index].selected;
    return Object.assign([], items);
};

const setSelectedBrazo = (items, index) => {
    items.forEach(element => {
        element.selected = false
    });
    items[index].selected = !items[index].selected;
    return Object.assign([], items);
};

const setSelectedExpresion = (items, index) => {
    items.forEach(element => {
        element.selected = false
    });
    items[index].selected = !items[index].selected;
    return Object.assign([], items);
};

const setSelectedpeinado = (items, index) => {
    items.forEach(element => {
        element.selected = false
    });
    // items.forEach(item => {
    //     item.id === id ? item.selected = true : item.selected = false
    // });
    items[index].selected = !items[index].selected;
    return Object.assign([], items);
};

const setSelectedLocation = (locations, id) => {
    return locations.map((location) => {
        location.selected = location.locationId === id;
        return location;
    });
};

export default personaje;