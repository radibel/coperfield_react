import { combineReducers } from "redux";
import personaje from "./PersonajeReducer";

const rootReducer = combineReducers({
    personaje,
});

export default rootReducer;